import firebase from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyB_IE3nUF9pFKmxYognGKVWxXnudW-Pm-Y",
  authDomain: "tenedores-3f367.firebaseapp.com",
  projectId: "tenedores-3f367",
  storageBucket: "tenedores-3f367.appspot.com",
  messagingSenderId: "646415695888",
  appId: "1:646415695888:web:c997b940b391f8eae20a95",
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
